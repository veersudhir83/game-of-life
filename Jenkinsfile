#!/usr/bin/env groovy

/**
 * @ Maintainer sudheer veeravalli<veersudhir83@gmail.com>
 * Plugins Needed in Jenkins: Maven, PMD, JUnit, Jacoco, HTML Publisher, 
 */

/* Only keep the 20 most recent builds. */
def projectProperties = [
        buildDiscarder(logRotator(artifactDaysToKeepStr: '20', artifactNumToKeepStr: '20', daysToKeepStr: '20', numToKeepStr: '20')),
        [$class: 'GithubProjectProperty', displayName: '', projectUrlStr: 'https://gitlab.com/veersudhir83/game-of-life.git'],
        // pipelineTriggers([pollSCM('H/10 * * * *')])
]

properties(projectProperties)

try {
    node {
        def mvnHome
        def antHome

        def appName = 'game-of-life'// application name currently in progress
        def appEnv  // application environment currently in progress
        def artifactName = appName // name of the war/jar/ear in artifactory
        def artifactExtension = "jar" // extension of the war/jar/ear - for both target directory and artifactory
        def workspaceRoot = env.WORKSPACE

        stage('Tool Setup'){
            // ** NOTE: These tools must be configured in the jenkins global configuration.
            try {
                if (isUnix()) {
                    sh "echo 'Running in Unix mode'"
                } else {
                    bat(/echo 'Running in windows mode' /)
                }
                mvnHome = tool name: 'mvn', type: 'maven'
            } catch (exc) {
                error "Failure in Tool Setup stage: ${exc}"
            }
        }

        stage('Checkout') {
            try {
                //cleanWs() // cleanup workspace before build starts

                // Checkout codes from repository
                dir('game-of-life') {
                    git url: 'https://gitlab.com/veersudhir83/game-of-life.git',
                            branch: 'master'
                }
            } catch (exc) {
                error "Failure in Checkout stage: ${exc}"
            }
        }

        stage('Build') {
            try {
                dir('game-of-life/') {
                    sh "'${mvnHome}/bin/mvn' clean package"
                    // sh "cp ./target/${appName}*.${artifactExtension} ./target/${appName}.${artifactExtension}"
                }
            } catch (exc) {
                error "Failure in Build stage: ${exc}"
            }
        }

        stage('Test & Analysis') {
            try {

                dir('game-of-life/') {
                    sh "'${mvnHome}/bin/mvn' test pmd:pmd"
                    // -P metrics pmd:pmd
                }
            } catch (exc) {
                error "Failure in Analysis stage: ${exc}"
            }
        }

        stage('Generate Reports') {
            try {
                jacoco()
                junit '**/game-of-life/gameoflife-web/target/surefire-reports/*.xml'
                pmd canComputeNew: false, defaultEncoding: '', healthy: '', pattern: '**/game-of-life/gameoflife-web/target/pmd.xml', unHealthy: ''
                publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'game-of-life/target/easyb', reportFiles: 'easyb.html', reportName: 'Test Report', reportTitles: ''])
            } catch (exc) {
                error "Failure in Generate Reports stage: ${exc}"
            }
        }

        stage('Archive Artifacts') {
            try {
                archiveArtifacts 'game-of-life/gameoflife-web/target/gameoflife.war'
            } catch (exc) {
                error "Failure in Archiving Artifacts stage: ${exc}"
            }
        }

        stage('Deploy on Slave Docker') {
            try {
                sh'''
                    // Stop and remove existing containers if any
                    ssh edureka@192.168.1.101 "docker ps -a | grep tomcat_8: | awk '{print $1}' | xargs docker rm -fv || exit 0"
                    
                    // Force remove any previous images from previous builds - $3 corresponds to image id
                    sh "docker images | grep tomcat_8 | awk '{print \$3}' | xargs docker rmi -f || exit 0"
                    sleep 10s
                    
                    cp $WORKSPACE/game-of-life/gameoflife-web/target/gameoflife.war /home/edureka/game_of_life/post_build_action
                    cd /home/edureka/game_of_life/post_build_action/
                    
                    scp gameoflife.war edureka@192.168.1.101:~/game_of_life/post_build_action
                    ssh edureka@192.168.1.101 "cd /home/edureka/game_of_life/post_build_action/;docker build -t tomcat_8:$BUILD_ID ."
                    ssh edureka@192.168.1.101 "docker run -d --name=tomcat_8-$BUILD_ID -p 9091:8080 tomcat_8:$BUILD_ID"
                '''
            } catch (exc) {
                error "Failure while deploying to docker container on slave: ${exc}"
            }
        }

    }
} catch (exc) {
    error "Caught: ${exc}"
}
